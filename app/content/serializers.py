from rest_framework import serializers

from content.models import Page, ContentItem, Video, Audio, Text


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ('title', 'url')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('url', 'srt_url')


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = ('url', 'bitrate')


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = ('text',)


class ContentSerializer(serializers.ModelSerializer):
    def _get_serializer_for_content_type(self, content_type):
        serializer = None
        model = content_type.model
        if model == 'video':
            serializer = VideoSerializer()
        elif model == 'audio':
            serializer = AudioSerializer()
        elif model == 'text':
            serializer = TextSerializer()

        if not serializer:
            raise NotImplementedError('Please provide a serializer for {} model'.format(model))
        return serializer

    def to_representation(self, instance):
        ct = instance.content_type
        serializer = self._get_serializer_for_content_type(ct)
        repr = serializer.to_representation(instance.content_object)
        repr['title'] = instance.title
        repr['type'] = ct.model
        return repr

    class Meta:
        model = ContentItem
        fields = ('pk', 'object_id', 'title')


class PageDetailsSerializer(serializers.ModelSerializer):
    contents = ContentSerializer(
        many=True, read_only=True
    )

    def get_object(self):
        obj = super().get_object()
        print(obj.pk)
        return obj

    class Meta:
        model = Page
        fields = ('title', 'contents')

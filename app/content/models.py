from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class Page(models.Model):
    title = models.CharField(
        max_length=250
    )

    def __str__(self):
        return self.title


class ContentItem(models.Model):
    page = models.ForeignKey(
        Page,
        related_name='contents',
        on_delete=models.CASCADE
    )
    title = models.CharField(
        max_length=250
    )
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        limit_choices_to={"model__in": ("video", "audio", "text")},
    )
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    counter = models.PositiveIntegerField(
        default=0,
        editable=False
    )

    def __str__(self):
        return self.title


class Video(models.Model):
    url = models.URLField(

    )

    srt_url = models.URLField(
        verbose_name='Link to subtitles',
        null=True, blank=True
    )


class Audio(models.Model):
    url = models.URLField(

    )

    bitrate = models.URLField(
        verbose_name='Audio bitrate',
        null=True, blank=True
    )


class Text(models.Model):
    text = models.TextField(

    )

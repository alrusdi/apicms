from rest_framework import viewsets
from content.models import Page
from content.serializers import PageSerializer, PageDetailsSerializer
from content.tasks import increment_view_counters


class PageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def get_serializer(self, *args, **kwargs):
        if not kwargs.get('many'):
            # This means details view of a page
            self.serializer_class = PageDetailsSerializer
            increment_view_counters.delay(args[0].pk)
        return super(PageViewSet, self).get_serializer(*args, **kwargs)

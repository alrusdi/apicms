from django.contrib import admin
from django.contrib.admin import StackedInline
from django.db.models import Q

from gfklookupwidget.widgets import GfkLookupWidget

from content import models


class ContentItemInline(StackedInline):
    model = models.ContentItem
    extra = 0

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'object_id':
            kwargs['widget'] = GfkLookupWidget(
                content_type_field_name='content_type',
                parent_field=models.ContentItem._meta.get_field('content_type'),
            )

        return super().formfield_for_dbfield(db_field, **kwargs)


@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
    inlines = (
        ContentItemInline,
    )
    search_fields = ('title', )

    def get_search_results(self, request, queryset, search_term):
        qs1 = Q(
            title__istartswith=search_term
        )

        qs2 = Q(
            contents__title__istartswith=search_term
        )

        qs = models.Page.objects.filter(qs1 | qs2)

        return qs, True


@admin.register(models.Video)
class VideoAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Audio)
class AudioAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Text)
class TextAdmin(admin.ModelAdmin):
    pass

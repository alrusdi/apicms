from django.test import TestCase
from rest_framework.test import APIClient


class PagesListView(TestCase):
    client_class = APIClient
    fixtures = ['content_fixture.json']

    def test_returns_correct_pages_list(self):
        pages_list_url = '/pages/?format=json'

        ret = self.client.get(pages_list_url)
        data = ret.json()

        self.assertEqual(ret.status_code, 200)

        self.assertIn(
            '/pages/1/',
            data['results'][0]['url']
        )

        self.assertEqual(
            'Page 1',
            data['results'][0]['title']
        )

    def test_returns_correct_page_details(self):
        pages_list_url = '/pages/1/?format=json'

        ret = self.client.get(pages_list_url)
        data = ret.json()

        self.assertEqual(ret.status_code, 200)

        self.assertIn('contents', data)
        self.assertIn('Page 1', data['title'])

        for ct in data['contents']:
            if ct['type'] == 'audio':
                self.assertIn('audio.url', ct['url'])
                self.assertIn('bitrate', ct)
            if ct['type'] == 'text':
                self.assertIn('Hello text', ct['text'])
                self.assertIn('title', ct)
            if ct['type'] == 'video':
                self.assertIn('video.url', ct['url'])
                self.assertIn('srt_url', ct)
                self.assertIn('title', ct)

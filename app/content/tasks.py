from celery import shared_task
from django.db.models import F
from .models import ContentItem


@shared_task()
def increment_view_counters(page_id):
    ContentItem.objects.filter(page_id=page_id).update(counter=F('counter') + 1)

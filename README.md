# apicms

Get list of the pages:

```
curl http://localhost/pages/
```

```
{"count":2,"next":"http://localhost/pages/?limit=1&offset=1","previous":null,"results":[{"title":"Page 1","url":"http://localhost/pages/1/"}]}
```

Get page details:

```
curl http://localhost/pages/1/
```

```
{"count":2,"next":"http://localhost/pages/?limit=1&offset=1","previous":null,"results":[{"title":"Page 1","url":"http://localhost/pages/1/"}]}
```
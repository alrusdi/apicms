#!/bin/bash
sleep 10

python3 manage.py migrate
python3 manage.py loaddata content_fixture

service supervisor start
service nginx start
tail -f /dev/null
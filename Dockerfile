FROM ubuntu:18.04

RUN export DEBIAN_FRONTEND=noninteractive \
    && sed -i 's,http://archive.ubuntu.com/ubuntu/,mirror://mirrors.ubuntu.com/mirrors.txt,' /etc/apt/sources.list \
    && apt-get update -qq && apt-get upgrade -qq \

    && apt-get install -y --no-install-recommends \
        nginx-light \
        python3 \
        python3-pip \
        python3-setuptools \
        python3-psycopg2 \
        supervisor \

    && BUILD_DEPS='build-essential python3-dev' \
    && apt-get install -y --no-install-recommends ${BUILD_DEPS} \

    && apt-get autoremove -y ${BUILD_DEPS} \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY app/requirements.txt /opt/apicms/app/
RUN pip3 install --no-cache-dir -r /opt/apicms/app/requirements.txt

COPY etc/ /etc/
COPY app/ /opt/apicms/app/

WORKDIR /opt/apicms/app
ENV STATIC_ROOT=/opt/apicms/static
RUN nginx -t \
    && python3 -c 'import compileall, os; compileall.compile_dir(os.curdir, force=1)' > /dev/null \
    && python3 ./manage.py collectstatic --settings=apicms.settings --no-input -v0

COPY docker_entrypoint.sh /opt/apicms/app
CMD /opt/apicms/app/docker_entrypoint.sh

